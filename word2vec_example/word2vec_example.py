import numpy as np
import pandas as pd
from gensim.models import Word2Vec
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline

from normalizer import Normalizer


class MeanEmbeddingVectorizer(object):
    def __init__(self, word2vec):
        self.word2vec = word2vec
        self.dim = len(list(word2vec.values())[0])

    def fit(self, X, y):
        return self

    def transform(self, X):
        return np.array([
            np.mean([self.word2vec[w] for w in words if w in self.word2vec]
                    or [np.zeros(self.dim)], axis=0)
            for words in X
        ])


csvs = [
    {'file': 'positive_1000.csv', 'label': 'positive'},
    {'file': 'negative_1000.csv', 'label': 'negative'}
]


# Предобработка данных и возвращение DataFrame
def make(list_of_csv: list, as_string: bool):
    data_frames = []
    normalizer = Normalizer()

    for csv in list_of_csv:
        df = pd.read_csv(csv['file'], sep=";", usecols=[3], names=['Текст'], header=None)
        df['label'] = [csv['label']] * len(df)
        df['lemmas'] = [''] * len(df)
        # лемматизируем
        for index, row in df.iterrows():
            df['lemmas'][index] = normalizer.clean_text(row['Текст'], as_string)

        data_frames.append(df)

    df = pd.concat(data_frames, ignore_index=True)

    # df.to_excel("dataframe.xlsx", engine='xlsxwriter')

    return df


#data = make(csvs, as_string=False)

data = pd.read_csv('dataset.csv')
data = data.drop(data.columns[[0, 1, 3]], axis=1)
data = data.rename(columns={"topics":"label", "text":"Текст"})

normalizer = Normalizer()
data['lemmas'] = [''] * len(data)
# лемматизируем
for index, row in data.iterrows():
    data['lemmas'][index] = normalizer.clean_text(row['Текст'], False)
    print(index)

print(data)

data.to_csv('clean_dataset.csv')

x_train, x_test, y_train, y_test = train_test_split(data['lemmas'], data['label'])
w2v_model = Word2Vec(size=300, min_count=1, workers=4, sg=1)
w2v_model.build_vocab(x_train)
w2v_model.train(x_train, total_examples=w2v_model.corpus_count, epochs=30, report_delay=1)
w2v_model.init_sims(replace=True)

w2v = dict(zip(w2v_model.wv.index2word, w2v_model.wv.syn0))

pipeline = Pipeline([
    ("word2vec vectorizer", MeanEmbeddingVectorizer(w2v)),
    ("extra trees", RandomForestClassifier(n_estimators=20))])

pipeline.fit(x_train, y_train)
pred = pipeline.predict(x_test)
print("MeanVectorizer")
print(classification_report(y_test, pred))
