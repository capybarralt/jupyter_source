import nltk
from nltk.corpus import stopwords
from pymorphy2 import MorphAnalyzer


class Normalizer:

    def __init__(self) -> None:
        super().__init__()
        self.__tokenizer = nltk.TweetTokenizer()
        self.__lemmatizator = MorphAnalyzer()
        self.__stopwords = stopwords.words('russian')

    # токенизация текста
    def __tokenyze(self, text) -> list:
        return self.__tokenizer.tokenize(text)

    # удаление знаков препинания
    def __remove_punctuation(self, tokens) -> list:
        return [word for word in tokens if word.isalpha()]

    # приведение токенов к строчным буквам
    def __lower_tokens(self, tokens) -> list:
        return [word.lower() for word in tokens]

    # удаление стоп-слов
    def __remove_stopwords(self, lemmas) -> list:
        return [word for word in lemmas if word not in self.__stopwords and len(word) > 1]

    # лемматизация текста
    def __lemmatize(self, tokens, normal_form_only: bool) -> list:
        # берём наиболее вероятное значение слова [0]
        lemmas = []
        for token in tokens:
            if normal_form_only:
                lemmas.append(self.__lemmatizator.parse(token)[0].normal_form)
            else:
                lemmas.append(self.__lemmatizator.parse(token)[0])

        return lemmas

    # представить массив строкой
    def __list_as_string(self, arr):
        return ' '.join(arr)

    # очистка текста
    def clean_text(self, text, as_string: bool):
        tokens = self.__tokenyze(text)
        tokens = self.__remove_punctuation(tokens)
        tokens = self.__lower_tokens(tokens)
        lemmas = self.__lemmatize(tokens, normal_form_only=True)
        lemmas = self.__remove_stopwords(lemmas)
        if as_string:
            return self.__list_as_string(lemmas)
        else:
            return lemmas
